//
// Created by Максим Хорошев on 21.01.2024.
//

#ifndef IMAGE_TRANSFORMER_BMP_MAIN_H
#define IMAGE_TRANSFORMER_BMP_MAIN_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;                //Метка BM
    uint32_t bfileSize;             //Длина в байтах
    uint32_t bfReserved;            //Резерв
    uint32_t bOffBits;              //Смещение области данных
    uint32_t biSize;                //Длина BITMAP_INFO
    uint32_t biWidth;               //Ширина картинки
    uint32_t biHeight;              //Высота картинки
    uint16_t biPlanes;              //Число цветовый плоскостей
    uint16_t biBitCount;            //Бит на пиксел
    uint32_t biCompression;         //Тип сжатия
    uint32_t biSizeImage;           //Размер изображения в байтах
    uint32_t biXPelsPerMeter;       //Разрешение по горизонтали
    uint32_t biYPelsPerMeter;       //Разрешение по вертикали
    uint32_t biClrUsed;             //Количество используемых цветов
    uint32_t biClrImportant;        //Количество основных цветов
};
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MEMORY_ALLOCATION_ERROR,
    READ_INVALID_PIXEL_COUNT
    /* коды других ошибок  */
};
/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_ERROR_DATA,
    WRITE_INVALID_SIGNATURE,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum read_status bmp_reader(FILE* input, struct image** image);
enum write_status bmp_writer(FILE* output, struct image const* image);



#endif //IMAGE_TRANSFORMER_BMP_MAIN_H
