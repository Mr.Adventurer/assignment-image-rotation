//#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>



#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
struct image* create_image(uint64_t width, uint64_t height);

void destroy_image(struct image* img);
struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};
#endif //IMAGE_TRANSFORMER_IMAGE_H
