//
// Created by Максим Хорошев on 21.01.2024.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
struct image* image_rotate(const struct image* image);
#endif //IMAGE_TRANSFORMER_ROTATE_H
