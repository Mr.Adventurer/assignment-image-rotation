#include "../include/bmp_main.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    // check amount of arguments
    if (argc != 3) {
        fprintf(stderr,"Incorrect arguments\n");
        fprintf(stderr, "Write input.bmp and output.bmp, separated by 1 space\n");
        return 1;
    }

    //check if file is opened correctly
    FILE* input = fopen(argv[1], "rb");
    if (!input) {
        fprintf(stderr,"Error opening input file\n");
        return 1;
    }

    // Read image from file
    struct image* image = NULL;
    enum read_status read_result = bmp_reader(input, &image);
    fclose(input);


    switch (read_result)
    {
        case READ_OK:
            printf("The reading was successful\n");
            break;
        case READ_INVALID_SIGNATURE:
            printf("Could not open the file\n");
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            return 1;
        case READ_INVALID_BITS:
            printf("The data was read with an error\n");
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            return 1;
        case READ_INVALID_HEADER:
            printf("The title was read with an error\n");
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            return 1;
        case READ_MEMORY_ALLOCATION_ERROR:
            printf("No memory has been allocated\n");
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            return 1;
        case READ_INVALID_PIXEL_COUNT:
            printf("Invalid image size\n");
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            break;
    }
    // Rotate image
    struct image* rotated = image_rotate(image);
    if (!rotated) {
        fprintf(stderr,"Error rotating image\n");
        destroy_image(image);
        image = NULL;
        return 1;
    }


    // Open file for writing
    FILE* output = fopen(argv[2], "wb");
    if (!output) {
        fprintf(stderr,"Error opening output file: %s\n", argv[2]);
        destroy_image(image);
        destroy_image(rotated);
        image = NULL;
        rotated = NULL;
        return 1;
    }

    // Write image to file
    enum write_status write_result = bmp_writer(output, rotated);
    fclose(output);
    switch (write_result)
    {
        case WRITE_OK:
            printf("File was writed successful\n");
            break;
        case WRITE_HEADER_ERROR:
            printf("Response header error\n");
            destroy_image(image);
            break;
        case WRITE_ERROR_DATA:
            printf("Response data error\n");
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            return 1;
        case WRITE_INVALID_SIGNATURE:
            printf("Could not open the file\n");
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            return 1;
        case WRITE_ERROR:
            if (image) {
                destroy_image(image);
                image = NULL;
            }
            return 1;
    }

    // Free memory
    destroy_image(image);
    destroy_image(rotated);
    //fprintf(stdout,"Freed memory from images\n");

    fprintf(stdout,"Success\n");
    return 0;
}
