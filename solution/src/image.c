#include "../include/image.h"
#include <stddef.h>
#include <stdlib.h>

#include <stdio.h>

/**
 * Creates an image from given width and height sizes
 * @param width - Number of pixels in uint64_t
 * @param height - Number of pixels in uint64_t
 * @return empty image with given parameters
 */
struct image* create_image(uint64_t width, uint64_t height) {
    struct image* image = (struct image*) malloc(sizeof(struct image));
    if (!image) return NULL;
    image->width = width;
    image->height = height;
    image->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));
    if (!image->data) {
        free(image);
        return NULL;
    }
    return image;
}

/**
 * Frees memory of the image
 * @param img - pointer to the image to free memory from
 */
void destroy_image(struct image* img) {
    if (!img) return;
    if (img->data) {
        free(img->data);
    }
    free(img);
}
struct image* image_rotate(const struct image* image) {
    struct image* new_image = create_image(image->height, image->width);
    if (!new_image) return NULL;

    for (size_t y = 0; y < image->height; y++) {
        for (size_t x = 0; x < image->width; x++) {
            new_image->data[(new_image->width-y-1)+x * new_image->width] = image->data[x+y * image->width];
        }
    }

    return new_image;
}
