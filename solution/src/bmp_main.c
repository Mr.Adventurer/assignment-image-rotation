//
// Created by Максим Хорошев on 21.01.2024.
//
#include "../include/bmp_main.h"
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>
/**
 * Calculates the padding of a given image
 * @param image - Image to be given
 * @return padding value in bytes
 */
uint64_t padding_calculate(struct image const* image) {
    uint32_t padding = (4 - ((image->width * 3) % 4)) % 4;
    return padding;
}

/**
 * Calculates the size of a given image, without the header
 * @param image - Image to be given
 * @return size of the image in bytes
 */
size_t image_size_calculate(struct image const* image) {
    uint64_t padding = padding_calculate(image);
    return (image->width * sizeof(struct pixel) + padding) * image->height;
}


/**
 * Creates a header struct from given image
 * @param image - It is used for getting width, height and padding
 * @return A bmp header struct with filled info
 */


static struct bmp_header create_bmp_header(struct image const* image) {
    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfileSize = image_size_calculate(image) + sizeof(struct bmp_header); // same as SizeImage + header size
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = image_size_calculate(image);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}


/**
 * Reads a file, which should be a bmp and writes an image with the data form file
 * @param input - Opened file
 * @param image - Image to write on the read data
 * @return A read status
 */
 enum read_status bmp_reader(FILE* input, struct image** image){
    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, input) != 1){
        free(*image);
        return READ_INVALID_HEADER;}
    if (header.bfType != 0x4D42){
        free(*image);
        return READ_INVALID_SIGNATURE;}
    *image = create_image(header.biWidth, header.biHeight);
    if (!*image) {
        free(*image);
        return READ_MEMORY_ALLOCATION_ERROR;
    }
    uint32_t padding = padding_calculate(*image);
    for (size_t i = 0; i < header.biHeight; i++) {
        if (fread(&(*image)->data[i * header.biWidth], sizeof(struct pixel), header.biWidth, input) != header.biWidth){

            free(*image);
            return READ_INVALID_PIXEL_COUNT;}
        if (fseek(input, (long)padding, SEEK_CUR) != 0) {
            free(*image);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;

}

/**
 * Writes to a file with data from image
 * @param output - File to write on
 * @param image - Image from which data is read
 * @return A write status
 */
enum write_status bmp_writer(FILE* output, struct image const* image) {
    struct bmp_header header = create_bmp_header(image);
    if (fwrite(&header, sizeof(header), 1, output) != 1)

        return WRITE_HEADER_ERROR;
    uint32_t padding = padding_calculate(image);
    for (size_t i = 0; i < image->height; i++) {
        if (fwrite(&image->data[i * image->width], sizeof(struct pixel), image->width, output) != image->width) {
            return WRITE_ERROR_DATA;
        }
        uint32_t zero = 0;
        if (padding && fwrite(&zero, 1, padding, output) != padding)
            return WRITE_ERROR_DATA;
    }

    return WRITE_OK;
}

